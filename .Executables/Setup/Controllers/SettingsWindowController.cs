﻿namespace Dcs.SkinRandomizer.Setup.Controllers;

using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using Commands;
using ModulesScanner;
using Repository;
using Settings.Models;
using ViewModels;

public class SettingsWindowController : IController
{
    private readonly SettingsRepository _settingsRepository;
    private readonly Window _window;
    private readonly SettingsViewModel _settingsViewModel;

    public SettingsWindowController(SettingsRepository settingsRepository, Window window)
    {
        _settingsRepository = settingsRepository;
        _window = window;
        _settingsViewModel = new SettingsViewModel();
    }

    public Task StartControllerAsync()
    {
        _settingsViewModel.FromModel(_settingsRepository.LoadRatingsOrCreateNew());
        _settingsViewModel.SaveAndExitCommand = new RelayCommand(SaveAndExit);
        _settingsViewModel.ExitCommand = new RelayCommand(Close);
        _settingsViewModel.ScanCommand = new RelayCommand(Scan);
        _settingsViewModel.SelectDcsUserFilesDirCommand = new RelayCommand(SelectDcsUserFilesDirCommand);
        _settingsViewModel.SelectDcsInstallDirCommand = new RelayCommand(SelectDcsInstallDirCommand);
        RefreshVisibilityProperties();

        _window.DataContext = _settingsViewModel;

        _window.Show();

        return Task.CompletedTask;
    }

    private void SelectDcsUserFilesDirCommand()
    {
        if (TrySelectDir(_settingsViewModel.DcsWorldUserFiles, out string selectedDir))
        {
            _settingsViewModel.DcsWorldUserFiles = selectedDir;
            RefreshVisibilityProperties();
        }
    }

    private void SelectDcsInstallDirCommand()
    {
        if (TrySelectDir(_settingsViewModel.DcsWorldInstallPath, out string selectedDir))
        {
            _settingsViewModel.DcsWorldInstallPath = selectedDir;
            RefreshVisibilityProperties();
        }
    }

    private bool TrySelectDir(string initialLocation, out string selectedDir)
    {
        FolderBrowserDialog fbd = new FolderBrowserDialog();
        fbd.ShowNewFolderButton = true;
        if (!string.IsNullOrWhiteSpace(initialLocation))
        {
            fbd.SelectedPath = initialLocation;
        }

        DialogResult result = fbd.ShowDialog();
        selectedDir = fbd.SelectedPath;
        return result == DialogResult.OK && !string.IsNullOrWhiteSpace(selectedDir);
    }

    public Task StopControllerAsync()
    {
        return Task.CompletedTask;
    }

    private void RefreshVisibilityProperties()
    {
        _settingsViewModel.IsDcsInstallPathValid = IsSelectedDcsInstallPathValid();
        _settingsViewModel.IsDcsUserFilesPathValid = IsSelectedDcsUserPathValid();
        _settingsViewModel.IsScanEnabled = _settingsViewModel.IsDcsInstallPathValid && _settingsViewModel.IsDcsUserFilesPathValid;
        _settingsViewModel.IsSaveAndExitEnabled = _settingsViewModel.IsScanEnabled && _settingsViewModel.DetectedModules.Count > 0;
    }

    private bool IsSelectedDcsUserPathValid()
    {
        return Directory.Exists(_settingsViewModel.DcsWorldUserFiles)
               && File.Exists(Path.Combine(_settingsViewModel.DcsWorldUserFiles, @"Config\options.lua"));
    }

    private bool IsSelectedDcsInstallPathValid()
    {
        return Directory.Exists(_settingsViewModel.DcsWorldInstallPath)
               && File.Exists(Path.Combine(_settingsViewModel.DcsWorldInstallPath, @"bin\dcs.exe"));
    }

    private void SaveAndExit()
    {
        _settingsRepository.Save(_settingsViewModel.SaveToNewModel());
        Close();
    }

    private void Close()
    {
        _window.Close();
    }

    private void Scan()
    {
        ModuleScanner modulesScanner = new ModuleScanner();
        List<ModuleSettings> moduleSettings = new List<ModuleSettings>();
        moduleSettings.AddRange(modulesScanner.Scan(_settingsViewModel.DcsWorldInstallPath));
        moduleSettings.AddRange(modulesScanner.Scan(_settingsViewModel.DcsWorldUserFiles));

        _settingsViewModel.ReplaceModuleSettings(moduleSettings);
        RefreshVisibilityProperties();
    }
}