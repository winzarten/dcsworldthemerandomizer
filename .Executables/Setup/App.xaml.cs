﻿namespace Dcs.SkinRandomizer.Setup
{
    using System.Windows;
    using Controllers;
    using Repository;

    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override async void OnStartup(StartupEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            SettingsWindowController settingsWindowController = new SettingsWindowController(new SettingsRepository(), mainWindow);
            await settingsWindowController.StartControllerAsync();
            /*SettingsRepository settingsRepository = new SettingsRepository();
            Settings.Models.Settings settings = settingsRepository.LoadRatingsOrCreateNew();

            ModuleScanner modulesScanner = new ModuleScanner();
            List<ModuleSettings> moduleSettings = new List<ModuleSettings>();
            moduleSettings.AddRange(modulesScanner.Scan(settings.DcsWorldInstallPath));
            moduleSettings.AddRange(modulesScanner.Scan(settings.DcsWorldUserFiles));

            settings.DetectedModules = moduleSettings;

            settingsRepository.Save(settings);*/
        }
    }
}