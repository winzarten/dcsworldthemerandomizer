﻿namespace Dcs.SkinRandomizer.Setup.Converters;

using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

public class BooleanToOkColorConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        if (value is bool bValue && bValue)
        {
            return Brushes.Green;
        }
        else
        {
            return Brushes.Red;
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}