﻿namespace Dcs.SkinRandomizer.Setup.ViewModels;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Settings.Models;

public class SettingsViewModel : AbstractViewModel<Settings>
{
    private string _dcsWorldUserFiles;
    private string _dcsWorldInstallPath;
    private bool _useMultiThreadVersion;
    private ICommand _selectDcsInstallDirCommand;
    private ICommand _selectDcsUserFilesDirCommand;
    private bool _isScanEnabled;
    private bool _isSaveAndExitEnabled;
    private ICommand _saveAndExitCommand;
    private ICommand _exitCommand;
    private ICommand _scanCommand;
    private bool _isScanCommandEnabled;
    private bool _isDcsInstallPathValid;
    private bool _isDcsUserFilesPathValid;

    public SettingsViewModel()
    {
        DetectedModules = new ObservableCollection<ModuleSettingsViewModel>();
    }

    public ICommand ScanCommand
    {
        get => _scanCommand;
        set => SetProperty(ref _scanCommand, value);
    }

    public bool IsScanCommandEnabled
    {
        get => _isScanCommandEnabled;
        set => SetProperty(ref _isScanCommandEnabled, value);
    }

    public ICommand SelectDcsInstallDirCommand
    {
        get => _selectDcsInstallDirCommand;
        set => SetProperty(ref _selectDcsInstallDirCommand, value);
    }

    public ICommand SelectDcsUserFilesDirCommand
    {
        get => _selectDcsUserFilesDirCommand;
        set => SetProperty(ref _selectDcsUserFilesDirCommand, value);
    }

    public bool IsScanEnabled
    {
        get => _isScanEnabled;
        set => SetProperty(ref _isScanEnabled, value);
    }

    public bool IsSaveAndExitEnabled
    {
        get => _isSaveAndExitEnabled;
        set => SetProperty(ref _isSaveAndExitEnabled, value);
    }

    public ICommand SaveAndExitCommand
    {
        get => _saveAndExitCommand;
        set => SetProperty(ref _saveAndExitCommand, value);
    }

    public ICommand ExitCommand
    {
        get => _exitCommand;
        set => SetProperty(ref _exitCommand, value);
    }

    public ObservableCollection<ModuleSettingsViewModel> DetectedModules { get; }

    public string DcsWorldUserFiles
    {
        get => _dcsWorldUserFiles;
        set => SetProperty(ref _dcsWorldUserFiles, value);
    }

    public string DcsWorldInstallPath
    {
        get => _dcsWorldInstallPath;
        set => SetProperty(ref _dcsWorldInstallPath, value);
    }

    public bool UseMultiThreadVersion
    {
        get => _useMultiThreadVersion;
        set => SetProperty(ref _useMultiThreadVersion, value);
    }

    public bool IsDcsInstallPathValid
    {
        get => _isDcsInstallPathValid;
        set => SetProperty(ref _isDcsInstallPathValid, value);
    }

    public bool IsDcsUserFilesPathValid
    {
        get => _isDcsUserFilesPathValid;
        set => SetProperty(ref _isDcsUserFilesPathValid, value);
    }

    protected override void ApplyModel(Settings model)
    {
        DcsWorldUserFiles = model.DcsWorldUserFiles;
        DcsWorldInstallPath = model.DcsWorldInstallPath;
        UseMultiThreadVersion = model.UseMultiThreadVersion;

        ReplaceModuleSettings(model.DetectedModules);
    }

    public override Settings SaveToNewModel()
    {
        return new Settings()
        {
            DcsWorldInstallPath = DcsWorldInstallPath,
            DcsWorldUserFiles = DcsWorldUserFiles,
            UseMultiThreadVersion = UseMultiThreadVersion,
            DetectedModules = DetectedModules.Select(x => x.SaveToNewModel()).ToList()
        };
    }

    public void ReplaceModuleSettings(List<ModuleSettings> moduleSettings)
    {
        DetectedModules.Clear();
        foreach (ModuleSettings modelDetectedModule in moduleSettings.OrderBy(x => x.Name))
        {
            ModuleSettingsViewModel newViewModel = new();
            newViewModel.FromModel(modelDetectedModule);
            DetectedModules.Add(newViewModel);
        }
    }
}