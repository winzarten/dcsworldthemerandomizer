﻿namespace Dcs.SkinRandomizer.Setup.ViewModels;

using Dcs.SkinRandomizer.Setup.Settings.Models;

public class ModuleSettingsViewModel : AbstractViewModel<ModuleSettings>
{
    private string _name;
    private string _id;
    private bool _isEnabled;

    public string Name
    {
        get => _name;
        set => SetProperty(ref _name, value);
    }

    public string Id
    {
        get => _id;
        set => SetProperty(ref _id, value);
    }

    public bool IsEnabled
    {
        get => _isEnabled;
        set => SetProperty(ref _isEnabled, value);
    }

    protected override void ApplyModel(ModuleSettings model)
    {
        Name = model.Name;
        IsEnabled = model.IsEnabled;
        Id = model.Id;
    }

    public override ModuleSettings SaveToNewModel()
    {
        return new ModuleSettings()
        {
            Name = Name,
            Id = Id,
            IsEnabled = IsEnabled
        };
    }
}