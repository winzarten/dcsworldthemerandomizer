﻿
namespace Dcs.SkinRandomizer.Runner 
{
    using System.Diagnostics;
    using NLog;
    using Setup.Repository;
    using Setup.Settings.Models;

    internal class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static async Task Main(string[] args)
        {
            SettingsRepository settingsRepository = new SettingsRepository();
            Settings settings = settingsRepository.LoadRatingsOrCreateNew();
            string executablePath = settings.UseMultiThreadVersion ? Path.Combine(settings.DcsWorldInstallPath,@"bin-mt\DCS.exe") : Path.Combine(settings.DcsWorldInstallPath,@"bin\DCS.exe");

            string missionEditorSettingsLua = Path.Combine(settings.DcsWorldUserFiles, @"MissionEditor\settings.lua");

            if (!Path.Exists(missionEditorSettingsLua))
            {
                Logger.Error($"Unable to Find DCS Menu Settings - looking in {executablePath}");
                return;
            }

            ReplaceMenuTheme(missionEditorSettingsLua, settings.DetectedModules.Where(x => x.IsEnabled).ToList());

            if (!Path.Exists(executablePath))
            {
                Logger.Error($"Unable to Find DCS executable - looking in {executablePath}");
                return;
            }

            ProcessStartInfo startInfo = new ProcessStartInfo(executablePath)
            {
                WorkingDirectory = Path.GetDirectoryName(executablePath)
            };

            Console.WriteLine($"Starting DCS: '{executablePath}'");
            Process.Start(startInfo);
            Console.WriteLine($"This window will close after 10 seconds...");
            await Task.Delay(10000);
        }

        private static void ReplaceMenuTheme(string settingsFilePath, List<ModuleSettings> settingsDetectedModules)
        {
            if (settingsDetectedModules.Count == 0)
            {
                return;
            }

            try
            {
                File.Copy(settingsFilePath, settingsFilePath+".old", true);

                List<string> lines = File.ReadAllLines(settingsFilePath).ToList();
                int lineIndex =lines.FindIndex(x => x.Contains("[\"skinName\"]"));
                if (lineIndex == -1)
                {
                    Logger.Info("Unable to find skinName entry");
                    return;
                }

                Random random = new Random();
                int indexToUse = random.Next(0, settingsDetectedModules.Count);

                var moduleToUSe = settingsDetectedModules[indexToUse];

                string newLine = $"\t\t[\"skinName\"] = \"{moduleToUSe.Id}\",";
                lines[lineIndex] = newLine;

                File.Delete(settingsFilePath);
                File.WriteAllLines(settingsFilePath, lines);
                //Console.WriteLine($"Switching Theme to: '{moduleToUSe.Name}'");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Unable to Replace Skin");
            }
        }
    }
}

;