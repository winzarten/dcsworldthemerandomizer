﻿namespace Dcs.SkinRandomizer.Setup.Settings.Models;

using System.Xml.Serialization;

[Serializable]
public class Settings
{
    public Settings()
    {
        DcsWorldInstallPath = string.Empty;
        DcsWorldUserFiles = string.Empty;
        UseMultiThreadVersion = true;
        DetectedModules = new List<ModuleSettings>();
    }

    public string DcsWorldUserFiles { get; set; }
    public string DcsWorldInstallPath { get; set; }

    public bool UseMultiThreadVersion { get; set; }

    public List<ModuleSettings> DetectedModules { get; set; }
}