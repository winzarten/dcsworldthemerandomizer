﻿namespace Dcs.SkinRandomizer.Setup.Settings.Models;

using System.Xml.Serialization;

public class ModuleSettings
{
    public ModuleSettings(string name, string id, bool isEnabled)
    {
        Name = name;
        Id = id;
        IsEnabled = isEnabled;
    }

    public ModuleSettings()
    {
    }

    [XmlAttribute]
    public string Name { get; set; }

    [XmlAttribute]
    public string Id { get; set; }

    [XmlAttribute]
    public bool IsEnabled { get; set; }
}