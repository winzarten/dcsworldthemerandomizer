﻿namespace Dcs.SkinRandomizer.Setup.ModulesScanner;

using NLog;
using Settings.Models;

public class ModuleScanner
{
    private static readonly string[] PluginsSubdirectories = new[] { "aircraft", "tech" };

    private static readonly string[] TerrainSubdirectories = new[] { "terrains" };

    private static readonly Dictionary<string, ModuleSettings> SpecialCases = new Dictionary<string, ModuleSettings>()
    {
        { "Supercarrier", new ModuleSettings("Supercarrier", "Supercarrier1", true) }
    };

    //private static readonly string[] DisabledByDefault = new[] { "Tacview by Raia Software", "SAM Sites Asset Pack", "terrains" };
    private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

    public IEnumerable<ModuleSettings> Scan(string directory)
    {
        string modsDirectory = Path.Combine(directory, "Mods");
        return PluginsSubdirectories.SelectMany(x =>
        {
            string packageDirectory = Path.Combine(modsDirectory, x);
            return ScanPluginInternal(packageDirectory);
        }).Concat(TerrainSubdirectories.SelectMany(x =>
        {
            string packageDirectory = Path.Combine(modsDirectory, x);
            return ScanLuaTableInternal(packageDirectory);
        }));
    }

    private static string GetStringWithinParentis(string s)
    {
        int startIndex = s.IndexOf('\"') + 1;
        int endIndex = s.IndexOf('\"', startIndex + 1);
        return s.Substring(startIndex, endIndex - startIndex);
    }
    
     private IEnumerable<ModuleSettings> ScanLuaTableInternal(string directory)
    {
        List<ModuleSettings> moduleSettingsList = new List<ModuleSettings>();
        if (!Directory.Exists(directory))
        {
            return moduleSettingsList;
        }

        string[] dirs = Directory.GetDirectories(directory, "*", SearchOption.TopDirectoryOnly);
        foreach (string moduleDirectory in dirs)
        {
            string entryFile = Path.Combine(moduleDirectory, "entry.lua");
            if (!File.Exists(entryFile))
            {
                Logger.Info($"Directory {moduleDirectory} doesn't have entry.lua");
                continue;
            }

            Logger.Info($"Parsing {entryFile}");

            string id;
            string name;

            try
            {
                List<string> lines = File.ReadAllLines(entryFile).ToList();

                // Yep this is ugly as hell...
                string lineWithId = lines.FirstOrDefault(x => x.Trim().ToLower().Contains("['id']"));
                if (string.IsNullOrWhiteSpace(lineWithId))
                {
                    Logger.Info("File doesn't have self_id info");
                    continue;
                }

                id = GetStringWithinParentis(lineWithId) + "1"; //Terrains looks to have a 1 suffix

                if (SpecialCases.TryGetValue(id, out ModuleSettings moduleSettings))
                {
                    moduleSettingsList.Add(moduleSettings);
                    continue;
                }

                string lineWithDisplayName = lines.FirstOrDefault(x => x.Trim().ToLower().Contains("['localizedname']"));

                if (string.IsNullOrWhiteSpace(lineWithDisplayName))
                {
                    Logger.Info("File doesn't have local displayName info");
                    continue;
                }

                name = GetStringWithinParentis(lineWithDisplayName);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error while parsing file");
                continue;
            }

            if (string.IsNullOrWhiteSpace(name) && string.IsNullOrWhiteSpace(id))
            {
                continue;
            }

            moduleSettingsList.Add(new ModuleSettings(name, id, true));
        }

        return moduleSettingsList;
    }

    private IEnumerable<ModuleSettings> ScanPluginInternal(string directory)
    {
        List<ModuleSettings> moduleSettingsList = new List<ModuleSettings>();
        if (!Directory.Exists(directory))
        {
            return moduleSettingsList;
        }

        string[] dirs = Directory.GetDirectories(directory, "*", SearchOption.TopDirectoryOnly);
        foreach (string moduleDirectory in dirs)
        {
            string entryFile = Path.Combine(moduleDirectory, "entry.lua");
            if (!File.Exists(entryFile))
            {
                Logger.Info($"Directory {moduleDirectory} doesn't have entry.lua");
                continue;
            }

            Logger.Info($"Parsing {entryFile}");

            string id = string.Empty;
            string name = string.Empty;
            bool containsSkin = false;

            try
            {
                List<string> lines = File.ReadAllLines(entryFile).ToList();

                // Yep this is ugly as hell...
                string lineWithLocalSelf = lines.FirstOrDefault(x => x.Trim().ToLower().Contains("self_id"));
                if (!string.IsNullOrWhiteSpace(lineWithLocalSelf))
                {
                    id = GetStringWithinParentis(lineWithLocalSelf);
                }
                else if (!TryFindIdInFunctionCall(lines, out id))
                {
                    Logger.Info("File doesn't have self_id info");
                    continue;
                }

                if (SpecialCases.TryGetValue(id, out ModuleSettings moduleSettings))
                {
                    moduleSettingsList.Add(moduleSettings);
                    continue;
                }

                string lineWithDisplayName = lines.FirstOrDefault(x => x.Trim().ToLower().StartsWith("displayname"));

                if (string.IsNullOrWhiteSpace(lineWithDisplayName))
                {
                    Logger.Info("File doesn't have local displayName info");
                    continue;
                }

                name = GetStringWithinParentis(lineWithDisplayName);

                int skinsIndex = lines.FindIndex(x => x.ToLower().Trim().StartsWith("skins"));
                if (skinsIndex == -1)
                {
                    continue;
                }

                containsSkin = lines[skinsIndex + 1].Trim() == "{" && lines[skinsIndex + 2].Trim() == "{" && lines[skinsIndex + 3].ToLower().Contains("name");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error while parsing file");
                continue;
            }

            if (string.IsNullOrWhiteSpace(name) && string.IsNullOrWhiteSpace(id) && !containsSkin)
            {
                continue;
            }

            //+ "1"; ids looks to have a 1 suffix
            moduleSettingsList.Add(new ModuleSettings(name, id + "1", true));
        }

        return moduleSettingsList;
    }

    private bool TryFindIdInFunctionCall(List<string> lines, out string id)
    {
        string lineWithDeclarePlugin = lines.FirstOrDefault(x => x.Trim().StartsWith("declare_plugin"));
        if (string.IsNullOrWhiteSpace(lineWithDeclarePlugin))
        {
            id = string.Empty;
            return false;
        }

        id = GetStringWithinParentis(lineWithDeclarePlugin);
        return !string.IsNullOrWhiteSpace(id);
    }
}