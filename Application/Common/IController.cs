﻿namespace Dcs.SkinRandomizer.Setup
{
    using System.Threading.Tasks;

    public interface IController
    {
        Task StartControllerAsync();

        Task StopControllerAsync();
    }
}