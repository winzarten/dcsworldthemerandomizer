﻿namespace Dcs.SkinRandomizer.Setup.Repository;

using Settings;
using Settings.Models;

public class SettingsRepository : AbstractXmlRepository<Settings>
{
    protected override string RepositoryDirectory => ".";
    protected override string FileName => "Options.xml";
}