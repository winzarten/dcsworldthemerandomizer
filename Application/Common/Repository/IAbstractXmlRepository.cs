﻿namespace Dcs.SkinRandomizer.Setup.Repository
{
    public interface IAbstractXmlRepository<T> where T : class, new()
    {
        T LoadRatingsOrCreateNew();
        void Save(T toSave);
    }
}