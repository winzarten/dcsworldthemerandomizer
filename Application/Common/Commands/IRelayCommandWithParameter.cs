﻿namespace Dcs.SkinRandomizer.Setup.Commands
{
    using System.Windows.Input;

    public interface IRelayCommandWithParameter<in T> : ICommand
    {
        void Execute(T parameter);
    }
}