# DCS World Theme Randomizer

### Download Here : [Releases]( https://gitlab.com/winzarten/dcsworldthemerandomizer/-/releases)


## What it is?

DCS World Theme Randomizer is small app that changes the selected menu theme (background + music) each time DCS World is run. 

## How to Use
1. Download the latest 7zip file from the [release tab]( https://gitlab.com/winzarten/dcsworldthemerandomizer/-/releases).
1. Extract the archive to any location. The application will need write access to the directory it is extracted.
1. Start "Setup.exe"

![Screenshot1](/_githubStuff/Setup1.png)

4. Specify paths for the DCS Install Directory, and the DCS User Files directory.

![Screenshot2](/_githubStuff/Setup2.png)

5. Check/Uncheck the multithreading checkbox based on the version of DCS you are using.
1. Press the **"Scan for Modules"** button.

![Screenshot3](/_githubStuff/Setup3.png)

7. This should populate the modules list on the right. You can deselect modules you don't wont to be part of the 'random theme' pool.
1. Press **"Save and Close"**
1. Use **"Runner.exe"** application to start DCS. 

Runner.exe will replace the selected theme in the option config file for a random one, before starting DCS itself. From now on, you should be using Runner.exe for starting Dcs.

## How to Uninstall
Just delete the app with its directory. The application doesn't store any data anywhere else, and doesn't modify DCS in any way.


## License
Licensed under [MIT License.](https://gitlab.com/winzarten/dcsworldthemerandomizer/-/raw/main/LICENSE)

Developed using JetBrains Rider <a href="https://www.jetbrains.com/?from=SecondMonitor"><img src="/_githubStuff/jetbrains.png"  width="120"></a>
